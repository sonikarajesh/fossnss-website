module.exports = {
  siteMetadata: {
    title: `FOSSNSS • Free and Open Source Software cell (FOSS Cell) of NSSCE`,
    description: `The FOSSNSS (FOSS Cell) of NSS College of Engineering, Palakkad is a collective of students who like to explore and spread the open-source software and its ideology. Roots of this FOSS Cell date back to the year 2009. Leading the whole campus to the world of Freedom, Community and Free Software. Free software, Free Campus, Free Society.`,
    author: 'foss@nssce.ac.in',
    siteUrl: `https://fossnss.org`,
  },
  plugins: [
    "gatsby-plugin-postcss",
    "gatsby-plugin-image",
    "gatsby-plugin-sitemap",
    "gatsby-plugin-sass",
    "gatsby-plugin-mdx",
    "gatsby-transformer-sharp",
    "gatsby-plugin-sharp",
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        icon: "src/images/foss-icon.png",
      },
    },
    {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 750,
            },
          },
        ],
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: "images",
        path: "src/images/",
      },
      __key: "images",
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: "blog",
        path: "src/content/blog/",
      },
      __key: "blog",
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: "pages",
        path: "src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: "events",
        path: "src/content/events/",
      },
      __key: "events",
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: "members",
        path: "src/content/members/",
      },
      __key: "members",
    },
  ],
};
