import React from 'react';
import { Link } from 'gatsby';
import { graphql } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image'; // Changed import for GatsbyImage

import Header from '../components/header';
import SEO from '../components/seo';
import Footer from '../components/footer';

import '../styles/partials/layouts/_blog.scss';

export default function Template({ data }) {
    return (
        <div className='mainbody'>
            <SEO title='Blog' />
            <Header />
            <div className='site-content'>
                <div className="post-content">
                    <div className="blog-heading">
                        <h1 className="underline-small">Latest Posts</h1>
                    </div>
                    <div className='container'>
                        <div className='blog-cards'>
                            {data.allMarkdownRemark.edges.map(post => (
                                <Link className='link' to={post.node.frontmatter.path} key={post.node.id}>
                                    <div className='card'>
                                        <div className='cover'>
                                           <GatsbyImage
                                    className='blog-img'
                                    image={getImage(post.node.frontmatter.cover)}
                                    alt='' // Added alt attribute for accessibility
                                />
                                        </div>
                                        <div className='cardbody'>
                                            <h3>{post.node.frontmatter.title}</h3>
                                            <span>{post.node.frontmatter.desc}</span>
                                            <div className="post-author author-dec">
                                                <img
                                                    src={`https://github.com/${post.node.frontmatter.author}.png?size=70`}
                                                    alt={`Avatar of ${post.node.frontmatter.name}`}
                                                />
                                                <div>
                                                    <p className="authcolor"><i className="fa fa-pencil colored-icon-pink" aria-hidden="true"></i> {post.node.frontmatter.name}</p>
                                                    <p className="authcolor"><i className="fa fa-calendar-o colored-icon-ora" aria-hidden="true"></i> {post.node.frontmatter.datestring}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}

export const pageQuery = graphql`
    query BlogIndexQuery {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "*/blog/.*md$/" } }
            sort:  {  frontmatter: {date: DESC}}
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        path
                        cover {
                            publicURL
                            childImageSharp {
                           
                                    gatsbyImageData(layout: FULL_WIDTH)
                                  
                            }
                        }
                        desc
                        name
                        tag
                        title
                        date
                        datestring
                        author
                    }
                    timeToRead
                }
            }
        }
    }
`;

