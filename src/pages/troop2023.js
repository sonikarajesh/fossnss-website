import React from 'react';
import { graphql } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image'; // Changed import for GatsbyImage

import Header from '../components/header';
import SEO from '../components/seo';
import Footer from '../components/footer';

const SecondPage = ({ data }) => (
    <div className='mainbody'>
        <SEO title='Meet the Team' />
        <Header />
        <div className='member-mainbody'>
        <div className="member-heading">
                        <h1 class="underline-small">Meet Our Team</h1>
                </div>
            {/* <h3 className='member-heading'>Meet our Team</h3> */}
            <div className='scoordinator'>
                <div className='coordinator'>
                    <a href='https://scholar.google.co.in/citations?user=jT73QC0AAAAJ&hl=en'>
                        
                    <GatsbyImage
                                    className='profile-pic'
                                    image={getImage(data.imgStaffCo)}
                                    alt='' // Added alt attribute for accessibility
                                />
                        {/* <div className='profile-pic'></div> */}
                        <h5>Sindhu S</h5>
                        <h6>Staff Co-ordinator</h6>
                    </a>
                </div>
                <br />
            </div>
            <div
                className='site-content-members'
                style={{
                    display: 'flex',
                    justifyContent: 'center',
                }}>
                <div className='showmembers'>
                    {data.allMarkdownRemark.edges.map(member => (
                        <div key={member.node.id} className='member-card'>
                            <a href={member.node.frontmatter.url}>
                                <img
                                    src={member.node.frontmatter.avathar}
                                    alt={`Avathar of ${member.node.frontmatter.name} from Gitlab/Github`}
                                />
                                <h5>{member.node.frontmatter.name}</h5>
                                <h6>{member.node.frontmatter.designation}</h6>
                            </a>
                        </div>
                    ))}
                </div>
            </div>
        </div>
        <Footer />
    </div>
);

export const MembersQuery = graphql`
    query MemberIndexQuery2023 {
        allMarkdownRemark(
            filter: { fileAbsolutePath: { regex: "/members/2023/.*md$/" } }
            sort: { frontmatter: {date: ASC} }
        ) {
            edges {
                node {
                    id
                    frontmatter {
                        name
                        avathar
                        designation
                        url
                        dept
                        email
                        phone
                        skills
                    }
                }
            }
        }
        imgStaffCo: file(relativePath: { eq: "sindhu.jpeg" }) {
            childImageSharp {
                           
                gatsbyImageData(layout: FULL_WIDTH)
              
        
            }
        }
    }
`;

export default SecondPage;
