import React from 'react';
// import { Link } from "gatsby"

import SEO from '../components/seo';
import Header from '../components/header';
import HeroSect from '../components/hero-section';
import Footer from '../components/footer';
import TreeSection from '../components/tree-section';
import EventSection from '../components/event-section';
import MemCTA from '../components/cta';

const IndexPage = () => (
    <div className='mainbody'>
        <SEO title='Home' />
        <Header />
        <div className="homebody">
            <HeroSect />

            <TreeSection />

            <EventSection />

            <MemCTA />

        </div>

        <Footer />
    </div>
);

export default IndexPage;
