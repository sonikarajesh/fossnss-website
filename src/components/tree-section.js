import React from 'react';
import { StaticQuery, graphql } from 'gatsby';
import { GatsbyImage, getImage } from 'gatsby-plugin-image';

const  TreeSection = () => (
    <StaticQuery
        query={graphql`
            query TreeLocQuery {
                treeImgLoc: file(relativePath: { eq: "banyan.png" }) {
                    childImageSharp {
                           
                        gatsbyImageData(layout: FULL_WIDTH)
                      
                
                    }
                }
                rmsImgLoc: file(relativePath: { eq: "rms.jpg" }) {
                    childImageSharp {
                           
                        gatsbyImageData(layout: FULL_WIDTH)
                      
                
                    }
                }
            }
        `}
        render={data => (
            <div className='tree-sect'>
                <div className='tree-text'>
                    <h1 class="underline-small">Building Community Through Sharing</h1>
                </div>
                
                <div className="tree-stuff">
                    <div className='tree-imgcontainer'>
                        <GatsbyImage
                                    className='tree-img'
                                    image={getImage(data.treeImgLoc)}
                                    alt='' // Added alt attribute for accessibility
                                />
                    </div>
                    <div className="testimon">
                    <hr className="quote-line" />
                    <div className="testimonial-quote group right">
                        <div className="quote-container">
                            <div>
                                <blockquote>
                                    <p>Sharing knowledge is the most fundamental
                                    act of friendship. Because it is a way
                                    you can give something without loosing
                                        something.”</p>
                                </blockquote>
                                <cite>
                                    <span>Richard Stallman</span><br />
                                        Founder<br />
                                        Free Software Foundation
                                </cite>
                            </div>
                        </div>
                    </div>
                    <hr className="quote-line" />
                </div>
                </div>

            </div>
        )}
    />
);

export default TreeSection;